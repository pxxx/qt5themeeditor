# Qt5ThemeEditor

Super simple qt5ct color scheme editor.

![screenshot](https://gitlab.com/pxxx/qt5themeeditor/-/raw/main/.screenshots/editor_screenshots.jpg)

To run the program just run the jar-file with:
```bash:
java -jar Qt5ThemeEditor.jar
```
form the terminal or click on it in a graphical file manager.
Alternative you can compile it your self:
```bash:
kotlinc -include-runtime src/*.* src/themeing/* -d Qt5ThemeEditor.jar
```
