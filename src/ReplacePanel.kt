import themeing.TButton
import themeing.TPanel
import themeing.TTextField
import themeing.Theme
import java.awt.FlowLayout

class ReplacePanel(theme: Theme): TPanel(theme){
    val target: TTextField = TTextField(theme, "        ")
    val destination: TTextField = TTextField(theme, "        ")
    val replaceForTabButton: TButton = TButton( "replace for tab", theme)
    val replaceAllButton: TButton = TButton( "replace all", theme)

    init {
        super.setLayout(FlowLayout())
        super.add(target)
        super.add(destination)
        super.add(replaceForTabButton)
        super.add(replaceAllButton)
    }
}