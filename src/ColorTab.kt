import themeing.TPanel
import themeing.Theme
import java.awt.Color
import java.awt.GridLayout
import kotlin.math.ceil

class ColorTab(theme: Theme, initColors: List<Color> = emptyList()): TPanel(theme){

    private var colors: Array<ColorLine> = if (initColors.isNotEmpty()){
        Array(initColors.size) { ColorLine(theme, "color${it + 1}", initColors[it]) }
    } else Array(21) { ColorLine(theme, "color${it + 1}") }


    fun setColors(list: List<Color>) {
        for (i in list.indices) {
           colors[i].color = list[i]
            colors[i].updateInput()
        }
    }

    fun getColors(): List<Color> = colors.map { it.color }

    init {
        val row = ceil(colors.size.toDouble()/2).toInt()
        super.setLayout(GridLayout(row, 2))
        addColors()
    }

    private fun addColors() {
        val row = ceil(colors.size.toDouble()/2).toInt()
        for (i in 0 until row) {
            super.add(colors[i])
            if (i+row < colors.size)
                super.add(colors[i+row])
        }
    }
}