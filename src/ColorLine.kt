import themeing.TLabel
import themeing.TPanel
import themeing.TTextField
import themeing.Theme
import java.awt.Color
import java.awt.FlowLayout
import javax.swing.JComboBox


class ColorLine(
    private val theme: Theme,
    colorName: String,
    color_: Color = Color(0xff0000)
): TPanel(theme){

    var color: Color = color_
    private val namePreview: TLabel = TLabel(theme, colorName)
    private var s = getComboBox(listOf("custom"))
    private var input: TTextField = TTextField(theme)
    private var isCustomColor: Boolean = true

    fun setS(list: List<String>) {
        s = getComboBox(list)
    }

    init {
        super.setLayout(FlowLayout())
        super.add(namePreview)
        // super.add(s)
        super.add(input)
        updateInput()

        input.addActionListener {
            println("call action listener")
            input.text.toColorOrNull()?.let { color = it }
            updateInput()
        }
    }

    fun updateC() {
        if (s.selectedItem?.toString() == "custom") {
            input.isEnabled = true
            isCustomColor = true
            namePreview.foreground = theme.foreground
        } else {
            input.isEnabled = false
            isCustomColor = false
            namePreview.foreground = theme.foreground
            namePreview.foreground = theme.color3
            // TODO: set color
        }
        updateInput()
    }

    fun updateInput() {
        input.text = color.getHexString()
        input.foreground = color.getComplementaryColor()
        input.background = color
    }

    private fun getComboBox(list: List<String>): JComboBox<String> {
        val jcb = JComboBox<String>()
        list.forEach { jcb.addItem(it) }
        return jcb
    }
}

private fun Color.getComplementaryColor(): Color {
    var r = this.hashCode() and 255
    var g = this.hashCode() shr 8 and 255
    var b = this.hashCode() shr 16 and 255
    val a = this.hashCode() shr 24 and 255
    r = 255 - r
    g = 255 - g
    b = 255 - b
    return Color(r + (g shl 8) + (b shl 16) + (a shl 24))
}
