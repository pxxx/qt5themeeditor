import themeing.Theme
import java.awt.Color
import javax.swing.JTabbedPane

class Tabs(theme: Theme): JTabbedPane() {

    private var activeColors: ColorTab = ColorTab(theme)
    private var disabledColors: ColorTab = ColorTab(theme)
    private var inactiveColors: ColorTab = ColorTab(theme)

    init {
        super.add(activeColors, "active colors")
        super.add(disabledColors, "disabled colors")
        super.add(inactiveColors, "inactive colors")
    }

    fun getAllColors(): List<List<Color>> =
        listOf(activeColors.getColors(), disabledColors.getColors(), inactiveColors.getColors())

    fun setColors(colors: List<List<Color>>) {
        activeColors.setColors(colors[0])
        disabledColors.setColors(colors[1])
        inactiveColors.setColors(colors[2])
    }
}