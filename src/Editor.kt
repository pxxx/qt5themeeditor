import themeing.TPanel
import themeing.Theme
import java.awt.BorderLayout
import java.awt.Color
import java.io.File
import javax.swing.JFrame

class Editor: JFrame("Qt5CtThemeEditor") {

    private val theme: Theme = Theme(foreground = Color.WHITE, background = Color.black, color3 = Color.gray)
    private val mainPanel: TPanel = TPanel(theme)
    private var tabs = Tabs(theme)

    private val replacePanel: ReplacePanel = ReplacePanel(theme)
    private val controlPanel: ControlPanel = ControlPanel(theme)

    init {
        super.setDefaultCloseOperation(EXIT_ON_CLOSE)
        super.setBackground(theme.background)
        super.setLocationRelativeTo(null)
        super.setSize(500, 450)
        super.setVisible(true)
        super.setLayout(BorderLayout())

        mainPanel.layout = BorderLayout()
        super.add(mainPanel)
        mainPanel.add(controlPanel, BorderLayout.SOUTH)
        mainPanel.add(tabs, BorderLayout.NORTH)
        mainPanel.add(replacePanel)

        controlPanel.saveField.addActionListener {
            saveToFile(controlPanel.saveField.text)
        }
        controlPanel.loadField.addActionListener {
            if (File(getAbsolutePath(controlPanel.loadField.text)).exists()) loadTheme(controlPanel.loadField.text)
        }

        replacePanel.replaceForTabButton.addActionListener {
            replacePanel.target.text.toColorOrNull()?.let { tc ->
                replacePanel.destination.text.toColorOrNull()?.let { dc ->
                    val s: ColorTab = tabs.selectedComponent as ColorTab
                    s.setColors(s.getColors().map { c -> if (c.getHexString() == tc.getHexString()) dc else c })
                }
            }
        }
        replacePanel.replaceAllButton.addActionListener {
            replacePanel.target.text.toColorOrNull()?.let { tc ->
                replacePanel.destination.text.toColorOrNull()?.let { dc ->
                    tabs.setColors(tabs.getAllColors().map {
                        it.map { c -> if (c.getHexString() == tc.getHexString()) dc else c }
                    })
                }
            }
        }
    }

    private fun loadTheme(path: String) {
        if (File(getAbsolutePath(path)).isFile) {
            println("load theme: $path")
            tabs.setColors(getColorsFromFile(path))
        } else println("$path not a file")
    }

    private fun getColorsFromFile(path: String): List<List<Color>> =
        File(getAbsolutePath(path)).readText().removeMultiple(" ").replace(", ", ",")
            .split("\n").drop(1).map { it1 -> it1.split("#").drop(1).map {
                it.dropLast(1) } }.filter { it.isNotEmpty() }.map { it.map { x -> x.toColor() } }

    private fun saveToFile(path: String) {
        val colors = tabs.getAllColors()
        File(getAbsolutePath(path)).writeText(
            "[ColorScheme]\n"+
                    "active_colors=${getStringFromColorList(colors[0])}\n"+
                    "disabled_colors=${getStringFromColorList(colors[1])}\n"+
                    "inactive_colors=${getStringFromColorList(colors[2])}"
        )
    }

    private fun getStringFromColorList(list: List<Color>): String =
        list.map { "#${Integer.toHexString(it.hashCode())}" }.toString().drop(1).dropLast(1)

    private fun getAbsolutePath(path_: String): String {
        var path = path_
        if (path[0] == '~') path = if (path[1] == '/') path.drop(2) else path.drop(1)
        path =  if (path[0] == '/') path else System.getProperty("user.home")+"/$path"
        return if (path.last()=='/') path.drop(1) else path
    }
}

fun main() {
    Editor()
}

fun String.toColor(): Color = Color(this.toLong(radix = 16).toInt(), this.length > 6)

fun String.toColorOrNull(): Color? {
    this.toLongOrNull(radix = 16)?.let {
        return Color(it.toInt(), this.length > 6)
    }
    return null
}

fun String.removeMultiple(remove: String): String {
    var out = this
    while (out.contains(remove+remove)) out = out.replace(remove+remove, remove)
    return out
}

fun Color.getHexString(): String {
    val hexString = Integer.toHexString(this.hashCode())
    return if (hexString.length == 8 && hexString.slice(0..1) == "ff") hexString.drop(2) else hexString
}