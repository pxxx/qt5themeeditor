package themeing

import javax.swing.BorderFactory
import javax.swing.JTextField

class TTextField (
    theme: Theme,
    text: String = ""
): JTextField (text){

    init {
        this.background = theme.background
        this.foreground = theme.foreground
        this.caretColor = theme.foreground
        this.border = BorderFactory.createLineBorder(theme.color3, 1)
    }
}