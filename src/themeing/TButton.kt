package themeing

import javax.swing.BorderFactory
import javax.swing.JButton
import javax.swing.UIManager


class TButton(
    text: String,
    theme: Theme
): JButton(){

    init {
        this.text = text
        this.foreground = theme.foreground
        this.border = BorderFactory.createLineBorder(theme.foreground)
        this.background = theme.background
        this.isFocusPainted = false

        UIManager.put("Button.select", theme.foreground)
    }
}