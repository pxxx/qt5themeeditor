package themeing

import javax.swing.JLabel

class TLabel(
    theme: Theme,
    text_: String = ""
): JLabel(text_) {
    init {
        this.foreground = theme.foreground
    }
}