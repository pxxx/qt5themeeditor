package themeing

import java.awt.Color

data class Theme(
    var foreground: Color = Color(0xffffff),
    var background: Color = Color(0x0),
    var color3: Color = Color (0xaaaaaa)
)