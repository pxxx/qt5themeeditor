package themeing

import javax.swing.JPanel

open class TPanel(
    theme: Theme
): JPanel(){

    init {
        this.background = theme.background
    }
}