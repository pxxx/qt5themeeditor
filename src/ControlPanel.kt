import themeing.TPanel
import themeing.TTextField
import themeing.Theme
import java.awt.FlowLayout

class ControlPanel(theme: Theme): TPanel(theme){
    val saveField = TTextField(theme, "path where to save the theme")
    val loadField = TTextField(theme, "path from where to load a theme")

    init {
        super.setLayout(FlowLayout())
        super.add(saveField)
        super.add(loadField)
    }
}